#!/bin/bash

docker build . -t soundy:latest
docker image tag soundy:latest 10.0.0.22:5000/soundy:latest
docker image push 10.0.0.22:5000/soundy:latest