#!/bin/bash

TAG=$(git describe --tags)

rm soundy.tgz

mkdir /tmp/soundy

cp -r * /tmp/soundy

tar --exclude=hashed.txt --exclude=.git --exclude=*.pyc --exclude=__pycache__ --exclude=.idea --exclude=soundy.tgz --exclude=.env --exclude=database/app.db -zcvf "soundy_${TAG}.tgz" -C /tmp/soundy/ .

rm -rf /tmp/soundy
