import datetime
from dateutil.relativedelta import relativedelta

STATUS_NEW = 'new'
STATUS_PENDING = 'pending'
STATUS_SUBMITTED = 'submitted'
STATUS_IN_PROGRESS = 'inprogress'
STATUS_FAILED = 'failed'
STATUS_CANCELED = 'canceled'
STATUS_COMPLETE = 'complete'

STATUS_TO_DISPLAY_MAP = {
    STATUS_NEW: 'New',
    STATUS_PENDING: 'Pending',
    STATUS_SUBMITTED: 'Submitted',
    STATUS_IN_PROGRESS: 'In Progress',
    STATUS_FAILED: 'Failed',
    STATUS_CANCELED: 'Canceled',
    STATUS_COMPLETE: 'Complete',
}

TICKET_STATUS_CHOICES = [
    (STATUS_NEW, "New"),
    (STATUS_SUBMITTED, "Submitted"),
    (STATUS_IN_PROGRESS, "In Progress"),
    (STATUS_CANCELED, "Cancelled"),
    (STATUS_COMPLETE, "Complete"),
]

TICKET_STATUS_FLOW_MAP = {
    STATUS_NEW: STATUS_SUBMITTED,
    STATUS_SUBMITTED: STATUS_IN_PROGRESS,
    STATUS_IN_PROGRESS: STATUS_COMPLETE,
    STATUS_COMPLETE: None,
}

TYPE_ACH = 'ach'
TYPE_CC = 'cc'

FREQUENCY_ONCE = 'once'
FREQUENCY_MONTHLY = 'monthly'
FREQUENCY_TWICE_MONTHLY = 'twice'
FREQUENCY_WEEKLY = 'weekly'
FREQUENCY_QUARTERLY = 'quarterly'
FREQUENCY_YEARLY = 'yearly'

FREQUENCY_OPTIONS = [
    FREQUENCY_ONCE,
    FREQUENCY_MONTHLY,
    FREQUENCY_TWICE_MONTHLY,
    FREQUENCY_WEEKLY,
    FREQUENCY_QUARTERLY,
    FREQUENCY_YEARLY,
]

FREQUENCY_TO_DISPLAY_MAP = {
    FREQUENCY_ONCE: "One Time",
    FREQUENCY_MONTHLY: "Monthly",
    FREQUENCY_TWICE_MONTHLY: "Twice a Month",
    FREQUENCY_WEEKLY: "Weekly",
    FREQUENCY_QUARTERLY: "Quarterly",
    FREQUENCY_YEARLY: "Yearly",
}


def add_monthly(d, multiplier=1):
    return d + relativedelta(months=+1 * multiplier)


def add_weekly(d, multiplier=1):
    return d + relativedelta(weeks=+1 * multiplier)


def add_twice_monthly(d, multiplier=1):
    return d + relativedelta(weeks=+2 * multiplier)  # TODO not right


def add_quarterly(d, multiplier=1):
    return d + relativedelta(months=+3 * multiplier)


def add_yearly(d, multiplier=1):
    return d + relativedelta(years=+1 * multiplier)


def add_once(d, multiplier=1):
    if multiplier == 1:
        return d
    return None


FREQUENCY_TO_FUCTION_MAP = {
    FREQUENCY_ONCE: add_once,
    FREQUENCY_MONTHLY: add_monthly,
    FREQUENCY_TWICE_MONTHLY: add_twice_monthly,
    FREQUENCY_WEEKLY: add_weekly,
    FREQUENCY_QUARTERLY: add_quarterly,
    FREQUENCY_YEARLY: add_yearly,
}


def increment_frequency(frequency, d, multiplier=1):
    func = FREQUENCY_TO_FUCTION_MAP.get(frequency, lambda: None)
    return func(d, multiplier)
