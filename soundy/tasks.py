from soundy import celery, db
from flask import current_app as app
from time import mktime
from datetime import datetime
import os
from soundy.models.song import Folder, Song
from chance_common.common.custom_logging import get_logger

RETRY_TIMES = 5

logger = get_logger(__name__)

file_extensions_audio = None
file_extensions_video = None


@celery.task
def process_transaction(transaction_id):
    from soundy import create_app
    app = create_app()
    with app.app_context():
        print('TODO')


def scan_folder_for_songs(folder_id):
    global file_extensions_audio, file_extensions_video
    if not file_extensions_audio:
        file_extensions_audio = app.config.get('VALID_AUDIO_EXTENSIONS', [])
    if not file_extensions_video:
        file_extensions_video = app.config.get('VALID_VIDEO_EXTENSIONS', [])

    folder = Folder.query.get(folder_id)
    for s in folder.songs:
        if not os.path.isfile(s.filepath):
            logger.debug(f"{s.filepath} was not found, deleting from database")
            db.session.delete(s)
    db.session.commit()
    folder = Folder.query.get(folder_id)

    if folder.type == 'video':
        file_extensions = file_extensions_video
    else:
        file_extensions = file_extensions_audio

    if folder and os.path.isdir(folder.filepath):
        logger.info(f"Scanning folder: {folder.id} path: {folder.filepath}")
        for subdir, dirs, files in os.walk(folder.filepath):
            for file in files:
                full_path = os.path.join(subdir, file)
                _, file_extension = os.path.splitext(full_path)
                try:
                    full_path.encode('utf-8')
                except UnicodeEncodeError:
                    logger.error(f"Failed to encode path {full_path} in utf-8, will be skipped")
                    full_path = None
                if full_path and file_extension in file_extensions:
                    logger.debug("Found file: " + full_path)
                    song = Song.query.filter((Song.filepath == full_path) | (Song.filename == file)).one_or_none()
                    if not song:
                        logger.debug("Creating song for path: " + full_path)
                        song = Song()
                        song.filename = file
                        song.filepath = full_path
                        song.folder_id = folder.id
                        song.size_mb = 123  # TODO
                        song.length_sec = 123
                        db.session.add(song)
                    else:
                        logger.debug("Song already exists")
            folder.last_scan = datetime.now()
            db.session.commit()
        return True
    else:
        logger.error(f"Failed to find folder for: {folder_id}")
        return False


if __name__ == '__main__':
    from soundy import create_app

    app = create_app()
    with app.app_context():
        print("Running task: ")
