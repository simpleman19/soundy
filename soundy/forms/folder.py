from flask_wtf import FlaskForm
from wtforms import StringField, SelectField
from wtforms.validators import DataRequired


class FolderForm(FlaskForm):
    filepath = StringField('Server Filepath', validators=[DataRequired()])
    name = StringField('Display Name', validators=[DataRequired()])
    type = SelectField('Folder Type', validators=[DataRequired()], choices=[('video', 'Video'), ('audio', 'Audio')])


class TagForm(FlaskForm):
    tag = StringField('Tag Name', validators=[DataRequired()])
