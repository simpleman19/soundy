from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms.compat import iteritems
from chance_common.common.forms import hydrate


class MoneyField(StringField):
    pass
