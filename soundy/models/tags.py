from . import db, BaseModel

tags = db.Table('tags',
                db.Column('tag_id', db.Integer, db.ForeignKey('tag.id'), primary_key=True),
                db.Column('song_id', db.Integer, db.ForeignKey('song.id'), primary_key=True)
                )


class Tag(BaseModel):
    __tablename__ = 'tag'

    tag = db.Column(db.String(250), nullable=False, unique=False)
    folder_id = db.Column(db.Integer, db.ForeignKey('folder.id'), nullable=False)
    songs = db.relationship('Song', secondary=tags, lazy=False, backref=db.backref('tags', lazy=False))

    def __init__(self, folder):
        super().__init__()
        self.folder_id = folder.id
