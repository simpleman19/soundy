from . import db, BaseModel


class Song(BaseModel):
    __tablename__ = 'song'
    excluded_fields = ['filepath']

    filename = db.Column(db.String(250), nullable=False, unique=True)
    size_mb = db.Column(db.Float, nullable=False)
    length_sec = db.Column(db.Float, nullable=False)
    folder_id = db.Column(db.Integer, db.ForeignKey('folder.id'), nullable=False)
    filepath = db.Column(db.String(500), nullable=False, unique=True)


class Folder(BaseModel):
    __tablename__ = 'folder'
    excluded_fields = ['filepath']

    filepath = db.Column(db.String(500), nullable=False, unique=True)
    name = db.Column(db.String(200), nullable=False)
    songs = db.relationship('Song', backref='folder', lazy=False)
    last_scan = db.Column(db.DateTime, nullable=True)
    tags = db.relationship('Tag', backref='folder', lazy=False)
    type = db.Column(db.String(20), nullable=False)
