from celery import Celery
from flask_migrate import Migrate

from chance_common.common.config import load_config
from chance_common.common.kernel import bootstrap_kernel
from .models import db
import config

migrate = Migrate()

celery_broker = 'redis://localhost:6379/1'

# print(celery_broker)

celery = Celery(__name__,
                broker=celery_broker,
                backend=celery_broker)
celery.config_from_object('celeryconfig')


def create_app(config_name=None):
    configuration = load_config(config_name)

    app = bootstrap_kernel(__name__, configuration, db, migrate)

    return app
