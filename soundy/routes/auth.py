import os
from flask import Blueprint, render_template, session, redirect, url_for, request, flash, current_app as app, g, \
    send_from_directory, make_response, abort
from werkzeug.utils import secure_filename
import datetime
from soundy.forms.auth import LoginForm
from soundy.forms.account import CreateAccountForm
from chance_common.common.auth import multi_auth, basic_auth, create_token, verify_password, get_token
from chance_common.common.custom_logging import get_logger
from chance_common.common.api_response import ApiResult, ApiException

from soundy.models import PasswordEntry

auth = Blueprint('auth', __name__, url_prefix='/authentication')

logger = get_logger(__name__)


@auth.route('/', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        if verify_password(form.email.data, form.password.data):
            remember_me = form.remember_me.data
            resp = make_response(redirect(url_for('main.index')))
            resp = get_token(resp, remember_me)
            return resp
        else:
            flash("Invalid credentials")
    resp = make_response(render_template('auth/login.jn2', form=form))
    resp.set_cookie('Bearer', '', expires=datetime.datetime.utcnow())
    return resp


@auth.route("/me", methods=['GET', 'POST'])
@multi_auth.login_required
def me():
    logger.info("Getting information about currently logged in user: %s", g.current_user_guid)
    user = PasswordEntry.query.filter_by(guid=g.current_user_guid).one_or_none()
    if user:
        response_dict = {'user': user.to_dict()}
        logger.info("Creating new token for user: %s", g.current_user_guid)
        response_dict['token'] = create_token(user_id=user.guid).decode('utf-8')
        return ApiResult(value=response_dict, status=200).to_response()
    else:
        raise ApiException(message="Unauthorized user", status=401)


@auth.route("/logout")
def logout():
    logger.info("Logging out user by clearing cookies and expiring them now")
    resp = redirect(url_for('main.index'))
    resp.set_cookie('Bearer', '', expires=datetime.datetime.now())
    session['login_failed'] = False
    return resp


@auth.errorhandler(ApiException)
def handle_api_exception(error: ApiException):
    return error.to_result().to_response()
