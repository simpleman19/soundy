import os
import re
from flask import Blueprint, render_template, redirect, url_for, request, flash, current_app as app, g, \
    send_from_directory, abort, send_file, Response

from chance_common.common.custom_logging import get_logger
from chance_common.common.api_response import ApiResult, ApiException
from chance_common.common.auth import token_auth
from chance_common.common.forms import hydrate

from soundy.models import db
from soundy.models.song import Folder, Song
from soundy.forms.folder import FolderForm, TagForm
from soundy.models.tags import Tag
from soundy.tasks import scan_folder_for_songs

main = Blueprint('main', __name__, url_prefix='')

root_path = os.path.dirname(os.path.abspath(__file__))

logger = get_logger(__name__)


@main.route('/static/<path:path>')
def send_js(path):
    return send_from_directory('static', path)


@main.route('/')
@token_auth.login_required
def index():
    folders = Folder.query.all()
    return render_template('index.jn2', folders=folders)


@main.route('/create_folder', methods=['GET', 'POST'])
@token_auth.login_required
def create_folder():
    form = FolderForm()
    if form.validate_on_submit():
        filepath = form.filepath.data
        if os.path.isdir(filepath):
            folder = Folder.query.filter_by(filepath=filepath).one_or_none()
            if not folder:
                folder = Folder()
            folder = hydrate(form, folder)
            db.session.add(folder)
            db.session.commit()
            return redirect(url_for('main.index'))
        else:
            flash(f"Failed to find path: {filepath}")
    return render_template('folder.jn2', form=form)


@main.route('/folder/<string:guid>')
@token_auth.login_required
def show_folder(guid):
    show_tagged = request.args.get('show_tagged', "true") == "true"
    search_string = request.args.get('query', "")
    folder = Folder.query.filter_by(guid=guid).one_or_none()
    songs = Song.query.filter_by(folder_id=folder.id).order_by(Song.filename.asc()).all()
    song_count = len(folder.songs)
    if search_string:
        songs = [s for s in songs if re.search(search_string, s.filename, re.IGNORECASE) or search_tags(search_string, s.tags)]
    if not show_tagged:
        songs = [s for s in songs if len(s.tags) == 0]
    if folder:
        return render_template('songs.jn2', f=folder, songs=songs, ss=search_string, song_count=song_count,
                               show_tagged=show_tagged)
    flash('Unable to find folder')
    return redirect(url_for('main.index'))


def search_tags(search_string, tags):
    for t in tags:
        if re.search(search_string, t.tag, re.IGNORECASE):
            return True
    return False


@main.route('/folder/<string:guid>/scan')
@token_auth.login_required
def scan_folder(guid):
    folder = Folder.query.filter_by(guid=guid).one_or_none()
    if folder:
        ret_val = scan_folder_for_songs(folder.id)
        return redirect(url_for('main.show_folder', guid=folder.guid))
    else:
        flash("Unable to find folder")
        return redirect(url_for('main.index'))


def scan_folder_api(guid):
    folder = Folder.query.filter_by(guid=guid).one_or_none()
    if folder:
        ret_val = scan_folder_for_songs(folder.id)
        if ret_val:
            return ApiResult({'success': True}, 200).to_response()
        else:
            return ApiResult({'success': False}, 201).to_response()
    raise ApiException("Unable to find folder")


@main.route('/song/<string:guid>')
@main.route('/song/<string:guid>/<string:filename>')
@token_auth.login_required
def play_song(guid, filename=None):
    song = Song.query.filter_by(guid=guid).one_or_none()
    return send_file(song.filepath, attachment_filename=song.filename, conditional=True)


@main.route('/folder/<string:folder_guid>/addtag', methods=['GET', 'POST'])
def add_tag(folder_guid):
    folder = Folder.query.filter_by(guid=folder_guid).one_or_none()
    form = TagForm()
    if form.validate_on_submit():
        tag = Tag(folder)
        hydrate(form, tag)
        db.session.add(tag)
        db.session.commit()
        return redirect(url_for('main.show_folder', guid=folder.guid))
    return render_template('add_tag.jn2', form=form, folder=folder)


@main.route('/song/<string:song_guid>/tag/<string:tag_guid>', methods=['POST'])
def tag_song(song_guid, tag_guid):
    song = Song.query.filter_by(guid=song_guid).one_or_none()
    tag = Tag.query.filter_by(guid=tag_guid).one_or_none()
    if song and tag:
        tag.songs.append(song)
        db.session.commit()
    if song:
        song = Song.query.filter_by(guid=song_guid).one_or_none()
        tags = [tag.tag for tag in song.tags]
    else:
        tags = []
    return ApiResult(value={'tags': ', '.join(tags)}).to_response()


@main.route('/health_check')
def health_check():
    return ApiResult(value={'status': 'healthy'}, status=208).to_response()


@main.errorhandler(ApiException)
def handle_api_exception(error: ApiException):
    return error.to_result().to_response()
