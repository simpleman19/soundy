import os
from datetime import datetime

from flask import Blueprint, render_template, session, redirect, url_for, request, flash, current_app as app, g, \
    send_from_directory, make_response, abort

from soundy.forms import hydrate
from soundy.forms.account import UpdateAccountForm, CreateAccountForm
from chance_common.common.custom_logging import get_logger
from soundy import db
from chance_common.common.auth import token_optional_auth, token_auth, get_token
from chance_common.common.models.auth import PasswordEntry
from chance_common.common.loaders import load_user_from_g, load_account_from_g_required
from chance_common.common.models.user import Account

account_bp = Blueprint('account', __name__, url_prefix='/account')

logger = get_logger(__name__)


@account_bp.route('/')
@token_auth.login_required
@load_account_from_g_required
def my_account():
    form = UpdateAccountForm(obj=g.account)
    return render_template('account/edit_account.jn2', form=form, account=g.account, disabled=True)


@account_bp.route('/create', methods=['GET', 'POST'])
@token_auth.login_required
@load_account_from_g_required
def create():
    if g.account.user_level < 300:
        raise 401
    logger.debug("Create page requested")
    form = CreateAccountForm()
    if form.validate_on_submit():
        logger.debug(f"Create form submitted for email: {form.email.data}")
        if not Account.query.filter_by(email=form.email.data).one_or_none():
            logger.debug(f"Attempting to make account for email: {form.email.data}")
            pw_entry = PasswordEntry(form.password.data)
            account = Account(password=pw_entry, email=form.email.data, first=form.first_name.data,
                              last=form.last_name.data)
            if form.is_admin.data:
                account.user_level = 999
            db.session.add(pw_entry)
            db.session.add(account)
            db.session.commit()
            logger.info(f"Created account for email: {form.email.data} with guid: {account.guid}")
            g.current_user_guid = account.guid
            return redirect(url_for('account.manage_accounts'))
        else:
            logger.warn(f"Account already exists with username: {form.email.data}")
            flash("An account already exists with that username address")
    resp = make_response(render_template('account/create.jn2', form=form))
    return resp


@account_bp.route('/update/<string:guid>', methods=['GET', 'POST'])
@token_auth.login_required
@load_account_from_g_required
def edit_account(guid):
    if g.account.user_level < 300:
        raise 401
    account = Account.query.filter_by(guid=guid).one_or_none()
    if not account:
        flash("Unable to find account")
        return redirect(url_for('account.manage_accounts'))
    form = UpdateAccountForm(obj=account)
    if form.validate_on_submit():
        account = hydrate(form, account)
        db.session.add(account)
        db.session.commit()
        return redirect(url_for('account.manage_accounts'))
    return render_template('account/edit_account.jn2', edit_account=account, form=form, disabled=False)


@account_bp.route('/delete/<string:guid>', methods=['GET', 'POST'])
@token_auth.login_required
@load_account_from_g_required
def delete_account(guid):
    if g.account.user_level < 300:
        raise 401
    account = Account.query.filter_by(guid=guid).one_or_none()
    if not account:
        flash("Unable to find account")
    else :
        db.session.delete(account)
        db.session.commit()
    return redirect(url_for('account.manage_accounts'))


@account_bp.route('/manage', methods=['GET', 'POST'])
@token_auth.login_required
@load_account_from_g_required
def manage_accounts():
    if g.account.user_level < 300:
        raise 401
    accounts = Account.query.all()
    return render_template('account/manage_accounts.jn2', account=g.account, accounts=accounts)
