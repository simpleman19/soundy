import os

from chance_common.common.config import config_class, DevelopmentConfig, ProductionConfig

basedir = os.path.abspath(os.path.dirname(__file__))
rootdir = os.path.join(basedir, 'soundy')


class SoundyMixIn(object):
    VALID_AUDIO_EXTENSIONS = ['.mp3', '.aac']
    VALID_VIDEO_EXTENSIONS = ['.mp4', '.ts']


@config_class('development')
class SoundyDevelopmentConfig(DevelopmentConfig, SoundyMixIn):
    def __init__(self, *args, **kwargs):
        self.BASE_PATH = basedir
        self.ROOT_PATH = rootdir
        super().__init__(*args, **kwargs)
        self.TEMPLATE_DIRS = [os.path.join(rootdir, 'templates')]


@config_class('production')
class SoundyProductionConfig(ProductionConfig, SoundyMixIn):
    def __init__(self, *args, **kwargs):
        self.BASE_PATH = basedir
        self.ROOT_PATH = rootdir
        super().__init__(*args, **kwargs)
        self.TEMPLATE_DIRS = [os.path.join(rootdir, 'templates')]
