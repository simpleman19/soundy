import os
import unittest
from time import sleep
from soundy import create_app, migrate
from manage import root_path
import flask_migrate
from selenium import webdriver
from config import load_config
import subprocess


class SeleniumBaseTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        config = load_config()
        if config.START_FLASK:
            print("Starting up flask")
            my_env = os.environ.copy()
            if not my_env.get("APP_CONFIG", None):
                my_env["APP_CONFIG"] = "uitesting"
            cls.p = subprocess.Popen(["python", "manage.py", "runserver"], stdout=subprocess.PIPE,
                                     stderr=subprocess.STDOUT, cwd=root_path, env=my_env)
        else:
            print("Not starting flask")

    def setUp(self):
        self.config = load_config()
        if self.config.WIPE_DATABASE:
            self.clear_db()
        self.driver = webdriver.Chrome(self.config.WEB_DRIVER_PATH)
        self.driver.get(self.config.BASE_URL)

    def tearDown(self) -> None:
        self.driver.close()

    @classmethod
    def tearDownClass(cls) -> None:
        config = load_config()
        if config.START_FLASK:
            print("Killing flask...")
            cls.p.kill()
            while True:
                line = cls.p.stdout.readline()
                if not line:
                    break
                print(line.decode().rstrip())
            cls.p.wait()
            print("Flask should be dead...")

    def clear_db(self):
        if os.path.exists(self.config.SQLITE_PATH):
            print("Removing database")
            os.remove(self.config.SQLITE_PATH)
        app = create_app()
        with app.app_context():
            flask_migrate.upgrade(directory=os.path.join(root_path, 'migrations'))
