import sys
import os
import subprocess
import contextlib
import getpass

from flask_migrate import MigrateCommand
from flask_script import Manager, Server, Command, Option
from livereload import Server as LiveServer

from soundy import create_app, db

from chance_common.common.models.user import Account
from chance_common.common.models.auth import PasswordEntry

manager = Manager(create_app)

port = os.environ.get('FLASK_PORT', 5000)

manager.add_command('db', MigrateCommand)
manager.add_command('runserver', Server(host="0.0.0.0", port=int(port)))

root_path = os.path.dirname(os.path.abspath(__file__))


def _make_context():
    return dict(app=manager.app, db=db)


class CeleryWorker(Command):
    """Starts the celery worker."""
    name = 'celery'
    option_list = (
        Option('--debug', '-d', dest='debug', default=False),
    )

    def run(self, debug=False):
        if debug:
            pass
        ret = subprocess.call(
            ['celery', 'worker', '-A', 'soundy.celery'])
        sys.exit(ret)


manager.add_command("celery", CeleryWorker())


class LiveCeleryWorker(Command):
    """Starts the celery worker."""
    name = 'celery'
    option_list = (
        Option('--debug', '-d', dest='debug', default=False),
    )

    def run(self, debug=False):
        if debug:
            pass
        ret = subprocess.call(
            ['watchmedo', 'auto-restart', '--directory=./', '--pattern=*.py', '--recursive', '--', 'celery', 'worker',
             '--app=soundy.celery', '--concurrency=1', '--loglevel=INFO'])
        sys.exit(ret)


manager.add_command("celery-live", LiveCeleryWorker())


@manager.option('-d', '--drop_first', help='Drop tables first?')
def createdb(drop_first=True):
    """Creates the database."""
    db.session.commit()
    if drop_first:
        print("Dropping all databases")
        db.drop_all()
    db.create_all()


@manager.command
def test():
    """Run unit tests"""
    tests = subprocess.Popen(['python', '-m', 'unittest'])
    tests.wait()
    with contextlib.suppress(FileNotFoundError):
        os.remove('files/test.db')


@manager.command
def resettobase():
    db.session.commit()
    db.drop_all()
    db.create_all()


@manager.command
def create_admin_user():
    username = input('Username: ')
    while Account.query.filter_by(email=username).one_or_none():
        print(f"Username: {username} already exists")
        username = input('Username: ')
    password = getpass.getpass(prompt='Password: ')
    pass_entry = PasswordEntry(password)
    account = Account(username, pass_entry, '', '')
    account.user_level = 999
    db.session.add(pass_entry)
    db.session.add(account)
    db.session.commit()


@manager.command
def create_default_admin_user():
    user = Account.query.filter_by(email='admin').one_or_none()
    if not user:
        pass_entry = PasswordEntry('Orocat12')
        account = Account('admin', pass_entry, '', '')
        account.user_level = 999
        db.session.add(pass_entry)
        db.session.add(account)
        db.session.commit()


@manager.command
def livereload():
    # app is a Flask object
    live_app = create_app()

    # remember to use DEBUG mode for templates auto reload
    # https://github.com/lepture/python-livereload/issues/144
    live_app.debug = True

    server = LiveServer(live_app.wsgi_app)
    # server.watch
    server.serve()


if __name__ == '__main__':
    manager.run()
