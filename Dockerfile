FROM python:3.8-slim

RUN apt-get update && apt-get install -y gcc

ARG APP_ROOT=/usr/src/app

EXPOSE 5000
EXPOSE 9000
VOLUME ["/usr/src/app/uploads"]

RUN mkdir -p ${APP_ROOT}
WORKDIR ${APP_ROOT}

COPY requirements.txt ${appRoot}
RUN pip install --no-cache-dir -r requirements.txt

ARG SERVICE_NAME
ARG SERVICE_VERSION
ENV SERVICE_NAME $SERVICE_NAME
ENV SERVICE_VERSION $SERVICE_VERSION

COPY . ${appRoot}

# Start gunicorn
CMD ["./entrypoint.sh"]