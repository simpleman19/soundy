#!/bin/bash -e

if ! command -v 'python3' &> /dev/null
then
    echo "python3 could not be found"
    exit
fi

if [ ! -d .env ]; then
  INSTALLING=true
  echo "Installing python virtualenv globally"
  sudo python3 -m pip install virtualenv

  echo "Creating virtual environment to run inside of"
  python3 -m virtualenv .env
else
  INSTALLING=false
  echo "Virtual environment already exists"
fi

PYTHON="$(pwd)/.env/bin/python3"

APP_CONFIG='production'
export APP_CONFIG

if [[ -z "$PORT" ]]; then
   PORT="9000"
fi

$PYTHON -m pip install -r requirements.txt

$PYTHON manage.py db upgrade

if [ "$INSTALLING" = true ] ; then
  echo "Create admin user: "
  $PYTHON manage.py create_admin_user
fi

WSGI="wsgi"
CMD_ARGS="--bind=0.0.0.0:${PORT} --workers=6"

GUNICORN_CMD_ARGS=${CMD_ARGS} .env/bin/gunicorn ${WSGI}:app
