#!/bin/bash -e

if [[ -z "$PORT" ]]; then
   PORT="9000"
fi

mkdir -p database

python manage.py db upgrade

echo "Create admin user: "
python manage.py create_default_admin_user

WSGI="wsgi"
CMD_ARGS="--bind=0.0.0.0:${PORT} --workers=6"

GUNICORN_CMD_ARGS=${CMD_ARGS} gunicorn ${WSGI}:app
